<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<dict>
    <entry key="blockstrategy">
        <dict>
            <entry key="name">
                <string>net.postchain.base.BaseBlockBuildingStrategy</string>
            </entry>
        </dict>
    </entry>
    <entry key="configurationfactory">
        <string>net.postchain.gtx.GTXBlockchainConfigurationFactory</string>
    </entry>
    <entry key="gtx">
        <dict>
            <entry key="modules">
                <array>
                    <string>net.postchain.rell.module.RellPostchainModuleFactory</string>
                    <string>net.postchain.gtx.StandardOpsGTXModule</string>
                </array>
            </entry>
            <entry key="rell">
                <dict>
                    <entry key="moduleArgs">
                        <dict>
                            <entry key="app">
                                <dict>
                                    <entry key="admin_pubkey">
                                        <bytea>039AD704EE107CF24C1EDF90B01937DAA754E2D7DD952765F3A5944993084E80C2</bytea>
                                    </entry>
                                </dict>
                            </entry>
                        </dict>
                    </entry>
                    <entry key="modules">
                        <array>
                            <string>app</string>
                        </array>
                    </entry>
                    <entry key="sources_v0.10">
                        <dict>
                            <entry key="app/auth.rell">
                                <string>
function require_admin() = require(
	is_signer(chain_context.args.admin_pubkey)
);
</string>
                            </entry>
                            <entry key="app/draw.rell">
                                <string>
entity winner {
	key lottery, wallet, ticket_id: integer; 
	key reward;
	key ticket_range;
}

function get_lottery_winners(lottery): list&lt;winner&gt; {
	// TODO: throw if draw wasn't performed yet
	return winner @* { lottery };
}

function get_lottery_winners_page(
	lottery, 
	page_size: integer, 
	after_rowid: integer
): (winners: list&lt;map&lt;name, gtv&gt;&gt;, count: integer, last_rowid: integer) {
	// TODO: throw if draw wasn't performed yet	
	val id = rowid.from_gtv(after_rowid.to_gtv());
	val winners = winner @* { 
		lottery,  
		.rowid &gt; id
	} ( 
		winner, 
		@omit @sort .rowid
	) limit page_size;
	
	if (winners.size() == 0) {
		return (
			winners = list&lt;map&lt;name, gtv&gt;&gt;(),
			count = 0,
			last_rowid = 0
		);
	}
	
	return (
		winners = map_winners(winners),
		count = winners.size(),
		last_rowid = integer.from_gtv(winners[winners.size() - 1].rowid.to_gtv())
	);
}

function draw(lottery) {
	// If number of rewards is greater than number of participants, we need to
	// keep track of users without rewards, otherwise we might end up in endless loop
	// by trying to distribute unassigned rewards, which cannot be assigned because
	// there are no more participants without rewards
	var lottery_participants_without_rewards_count = get_draw_participant_count(lottery);
	
	require(
		lottery.state == lottery_state.waiting_draw,
		"Lottery has to be in 'waiting draw' state in order to start the draw"
	);
		
	for (reward_spec in rewards_from_bytes(lottery.rewards)) {
		// No more participants without rewards, therefore stop the draw
		if (lottery_participants_without_rewards_count == 0) break;
		
		when (reward_spec.reward_spec_type) {
			reward_spec_type.range -&gt; {
				lottery_participants_without_rewards_count -= perform_draw_for_reward_range(
					lottery,				
					reward_spec.reward_type, 
					reward_range.from_gtv(reward_spec.details),
					lottery_participants_without_rewards_count
				);
			} 
			reward_spec_type.lst -&gt; {
				lottery_participants_without_rewards_count -= perform_draw_for_reward_list(
					lottery,				
					reward_spec.reward_type,
					reward_list.from_gtv(reward_spec.details),
					lottery_participants_without_rewards_count
				);
			}
			reward_spec_type.count -&gt; {
				lottery_participants_without_rewards_count -= perform_draw_for_reward_count(
					lottery,
					reward_spec.reward_type,
					_reward_count.from_gtv(reward_spec.details),
					lottery_participants_without_rewards_count
				);
			} 
		}
	}
	
	lottery.state = lottery_state.draw_finished;
}

function perform_draw_for_reward_range(lottery, reward_type, reward_range, max_reward_count: integer) {
	val ticket_count = get_ticket_count(lottery);
	
	var assigned_reward_count = 0;
	val reward_count_to_distribute = min(max_reward_count, reward_range.end - reward_range.start + 1);
	val reward_range_ids = range(reward_range.start, reward_range.end);
	
	while (assigned_reward_count &lt; reward_count_to_distribute) {
		val ticket_id = random_integer() % ticket_count;
		val ticket_range = TicketRange(lottery, ticket_id);
		
		if (is_eligible_for_reward(lottery, ticket_range.wallet)) {
			val reward = create reward(
				reward_type, 
				reward_range_id_details(reward_range.start + assigned_reward_count)
			);
			add_winner(lottery, ticket_range.wallet, ticket_range, reward, ticket_id);		
			assigned_reward_count++;	
		}
	}
	
	return assigned_reward_count;
}

function perform_draw_for_reward_list(lottery, reward_type, reward_list, max_reward_count: integer) {
	val ticket_count = get_ticket_count(lottery);
	
	var assigned_reward_count = 0;
	val reward_count_to_distribute = min(max_reward_count, reward_list.rewards.size());
	
	while (assigned_reward_count &lt; reward_count_to_distribute) {
		val ticket_id = random_integer() % ticket_count;
		val ticket_range = TicketRange(lottery, ticket_id);
		
		if (is_eligible_for_reward(lottery, ticket_range.wallet)) {
			val reward = create reward(
				reward_type,
				reward_id_details(reward_list.rewards[assigned_reward_count])
			);
			add_winner(lottery, ticket_range.wallet, ticket_range, reward, ticket_id);
			assigned_reward_count++;
		}
	}
	
	return assigned_reward_count;
}

function perform_draw_for_reward_count(lottery, reward_type, _reward_count, max_reward_count: integer) {
	val ticket_count = get_ticket_count(lottery);
	
	var assigned_reward_count = 0;
	val reward_count_to_distribute = min(max_reward_count, _reward_count.value);
	
	while (assigned_reward_count &lt; reward_count_to_distribute) {
		val ticket_id = random_integer() % ticket_count;
		val ticket_range = TicketRange(lottery, ticket_id);
		
		if (is_eligible_for_reward(lottery, ticket_range.wallet)) {
			val reward = create reward(reward_type, reward_empty_details());
			add_winner(lottery, ticket_range.wallet, ticket_range, reward, ticket_id);
			assigned_reward_count++;			
		}
	}
	
	return assigned_reward_count;
}

function add_winner(lottery, wallet, ticket_range, reward, ticket_id: integer) {
	create winner(lottery, wallet, reward, ticket_range, ticket_id);
}

function map_winner(winner): map&lt;name, gtv&gt; {
	val type = winner.reward.type;
	
	return [
		"lottery_id": winner.lottery.id.to_gtv(),
		"ticket_id": winner.ticket_id.to_gtv(),
		"address": winner.wallet.address.to_gtv(),
		"reward": map_reward(winner.reward).to_gtv()
	];
}

function map_winners(winners: list&lt;winner&gt;): list&lt;map&lt;name, gtv&gt;&gt; {
	val winners_maps = list&lt;map&lt;name, gtv&gt;&gt;();
	
	for (winner in winners) {
		winners_maps.add(map_winner(winner));
	}
	
	return winners_maps;
}
</string>
                            </entry>
                            <entry key="app/lottery.rell">
                                <string>
enum lottery_state {
	created,
	snapshotting,
	snapshotting_finished,
	paused,
	canceled,
	waiting_draw,
	draw_finished,
	finished
}

function Lottery(id: integer) = require(
	lottery @? { .id == id },
	"Lottery doesn't exist, ID = &lt;%d&gt;".format(id)
);

entity lottery {
	key id: integer;
	mutable state: lottery_state;
	index state;
	mutable participation_rules: byte_array;	
	mutable ticket_rules: byte_array;
	mutable rewards: byte_array;
	mutable description: text;
	mutable draw_time: timestamp;	
}

function get_lottery(id: integer): gtv? {
	val lottery = lottery @? { .id == id };
	
	return if (lottery??) lottery_to_gtv_pretty(lottery) else null;
}

function get_active_lottery(): lottery? {
	val active_lotteries = lottery @* { .state != lottery_state.canceled, .state != lottery_state.finished };
	
	require(
		active_lotteries.size() &lt; 2,
		"More than one lottery active"
	);
	
	return if (active_lotteries.size() &gt; 0) active_lotteries[0] else null;
}

function create_lottery(
	participation_rules: list&lt;participation_rule&gt;,	
	ticket_rules: list&lt;ticket_rule&gt;, 
	rewards: list&lt;reward_spec&gt;, 
	description: text,
	draw_time: timestamp
): lottery {
	require(
		(lottery @* { .state != lottery_state.canceled, .state != lottery_state.finished }).size() == 0,
		"Cannot create new lottery while an active lottery exists"
	);
	
	validate_participation_rules(participation_rules);	
	validate_ticket_rules(ticket_rules);
	validate_rewards(rewards);
	
	val round_count = (lottery @* {} ( .rowid )).size();
	
	return create lottery(
		id = round_count+1,
		state = lottery_state.created,
		participation_rules = participation_rules.to_gtv().to_bytes(),		
		ticket_rules = ticket_rules.to_gtv().to_bytes(),
		rewards = rewards.to_gtv().to_bytes(),
		description = description,
		draw_time = draw_time
	);
}

function update_lottery(
	lottery,
	participation_rules: list&lt;participation_rule&gt;,	
	ticket_rules: list&lt;ticket_rule&gt;, 
	rewards: list&lt;reward_spec&gt;, 
	description: text,
	draw_time: timestamp
) {
	require(
		lottery.state == lottery_state.created 
			or 
		lottery.state == lottery_state.snapshotting_finished 
			or 
		lottery.state == lottery_state.waiting_draw,
		"Cannot update lottery while in state: " + lottery.state
	);
	
	validate_participation_rules(participation_rules);	
	validate_ticket_rules(ticket_rules);
	validate_rewards(rewards);
	
	lottery.participation_rules = participation_rules.to_gtv().to_bytes();		
	lottery.ticket_rules = ticket_rules.to_gtv().to_bytes();
	lottery.rewards = rewards.to_gtv().to_bytes();
	lottery.description = description;
	lottery.draw_time = draw_time;
}

function allowed_state_transitions_for_state(state: lottery_state): list&lt;lottery_state&gt; {
	val transitions = [
		lottery_state.created: [
			lottery_state.snapshotting, 
			lottery_state.canceled
		],
		lottery_state.snapshotting: [
			lottery_state.paused, 
			lottery_state.canceled, 
			lottery_state.waiting_draw, 
			lottery_state.snapshotting_finished
		],
		lottery_state.snapshotting_finished: [
			lottery_state.waiting_draw, 
			lottery_state.canceled, 
			lottery_state.snapshotting
		],
		lottery_state.paused: [
			lottery_state.snapshotting
		],
		lottery_state.waiting_draw: [
			lottery_state.canceled, 
			lottery_state.draw_finished
		],
		lottery_state.draw_finished: [
			lottery_state.finished
		],
		lottery_state.canceled: list&lt;lottery_state&gt;(),
		lottery_state.finished: list&lt;lottery_state&gt;()
	];
	
	return transitions[state];
}

function set_lottery_state(lottery, state: lottery_state) {
	require(
		lottery.state != lottery_state.canceled and lottery.state != lottery_state.finished,
		"Cannot update canceled or finished lottery"
	);
	
	if (lottery.state == state) return;
	
	val allowed_states = allowed_state_transitions_for_state(lottery.state);
	
	require(
		allowed_states.contains(state),
		"Cannot transit from state '" + lottery.state + "' to state '" + state + "'"
	);
	
	lottery.state = state;
}

function update_lottery_description(lottery, description: text) {
	lottery.description = description;
}

function update_lottery_draw_time(lottery, draw_time: timestamp) {
	lottery.draw_time = draw_time;
}

function get_participant_count(lottery) =
	(ticket_range @* { lottery } (@group .wallet)).size();
	

function get_draw_participant_count(lottery): integer {
	val last_snapshot_round = require(
		current_snapshot_round(lottery)
	);
	
	return (ticket_range @* { 
		lottery, 
		.snapshot.round == last_snapshot_round
	} (@group .wallet)).size() ;
}

function lottery_to_gtv_pretty(lottery) = (
	id = lottery.id,
	state = lottery.state,
	participation_rules = participation_rules_from_bytes(lottery.participation_rules),
	ticket_rules = ticket_rules_from_bytes(lottery.ticket_rules),
	rewards = rewards_from_bytes(lottery.rewards),
	description = lottery.description,
	next_snapshot_time = current_snapshot_round(lottery)?.time ?: 0,
	draw_time = lottery.draw_time
).to_gtv_pretty();	
</string>
                            </entry>
                            <entry key="app/module.rell">
                                <string>
import .operations;
import .queries;

struct module_args {
	admin_pubkey: pubkey;
}
</string>
                            </entry>
                            <entry key="app/operations.rell">
                                <string>module;

import app;

function get_active_lottery() =
	require(app.get_active_lottery(), "No active lottery");

operation register_token(name, chain: name) {
	app.require_admin();
	create app.token(name, chain);
}

operation register_tokens(tokens: list&lt;(name, name)&gt;) {
	app.require_admin();
	for ((name, chain) in tokens) {
		create app.token(name, chain);
	}
}

operation register_master_wallet(address: text) {
	app.require_admin();
	app.register_master_wallet(address);
}

operation link_wallet(address: text, master_address: text) {
	app.require_admin();
	app.link_wallet(address, master_address);
}

operation unlink_wallet(address: text) {
	app.require_admin();		
	app.unlink_wallet(app.Wallet(address));
}

operation create_lottery(
	participation_rules: list&lt;app.participation_rule&gt;,
	ticket_rules: list&lt;app.ticket_rule&gt;, 
	rewards: list&lt;app.reward_spec&gt;, 
	description: text,
	draw_time: timestamp
) {
	app.require_admin();
	app.create_lottery(
		participation_rules, 
		ticket_rules, 
		rewards, 
		description, 
		draw_time
	);	
}

operation update_lottery(
	participation_rules: list&lt;app.participation_rule&gt;,
	ticket_rules: list&lt;app.ticket_rule&gt;, 
	rewards: list&lt;app.reward_spec&gt;, 
	description: text,
	draw_time: timestamp
) {
	app.require_admin();
	app.update_lottery(
		get_active_lottery(),
		participation_rules, 
		ticket_rules, 
		rewards, 
		description, 
		draw_time
	);		
}

operation set_lottery_state(state: text) {
	app.require_admin();	
	app.set_lottery_state(get_active_lottery(), app.lottery_state.value(state));
}

operation update_lottery_description(description: text) {
	app.require_admin();
	app.update_lottery_description(get_active_lottery(), description);
}

operation update_lottery_draw_time(draw_time: timestamp) {
	app.require_admin();
	app.update_lottery_draw_time(get_active_lottery(), draw_time);
}

operation create_new_snapshotting_round(time: timestamp) {
	app.require_admin();
	app.create_new_snapshotting_round(get_active_lottery(), time);
}

operation save_wallet_snapshot(address: text, balances: list&lt;app.token_balance&gt;) {
	app.require_admin();
	app.save_wallet_snapshot(get_active_lottery(), app.Wallet(address), balances);	
}

operation issue_tickets() {
	app.require_admin();	
	app.issue_tickets(get_active_lottery());
}

operation draw() {
	// be careful and make sure code is reliable and doesn't throw errors, otherwise we will not be able to perform draw 
	app.require_admin();
	app.draw(get_active_lottery());
}
</string>
                            </entry>
                            <entry key="app/participation.rell">
                                <string>
struct participation_rule {
	token_name: text;
	token_chain: text;
	token_amount: integer;
}

struct _participation_rule {
	token;
	amount: integer;
}

function validate_participation_rules(rules: list&lt;participation_rule&gt;) {
	// Can we have multiple rules for the same token?
	for (rule in rules) {
		val _ = Token(rule.token_name, rule.token_chain);
		require(rule.token_amount &gt; 0);		
	}
}

function participation_rules_from_bytes(rules: byte_array) =
	list&lt;participation_rule&gt;.from_gtv(gtv.from_bytes(rules));

function _participation_rules_from_bytes(rules: byte_array): list&lt;_participation_rule&gt; {
	val deserialized_rules = participation_rules_from_bytes(rules);
	val _rules = list&lt;_participation_rule&gt;();
	for (rule in deserialized_rules) {
		_rules.add(_participation_rule(
			Token(rule.token_name, rule.token_chain),
			rule.token_amount
		));
	}
	return _rules;
}

function _participation_rules_map_from_bytes(rules: byte_array): map&lt;token, integer&gt; {
	val rules_map = map&lt;token, integer&gt;();
	val deserialized_rules = participation_rules_from_bytes(rules);
	for (rule in deserialized_rules) {
		rules_map[Token(rule.token_name, rule.token_chain)] = rule.token_amount;		
	}
	return rules_map;
}
</string>
                            </entry>
                            <entry key="app/queries.rell">
                                <string>module;

import app;

query get_wallet_lottery_state(address: text) {
	val lottery = app.get_active_lottery();
	val status = app.get_wallet_status(address, lottery);
	
	if (empty(status)) return null;
	
	val rewards = list&lt;(
		lottery_id: integer, 
		ticket_id: integer, 
		reward: map&lt;text, gtv&gt;,
		timestamp: integer
	)&gt;();
	
	for ((lottery_id, ticket_id, reward, timestamp) in status.rewards) {
		rewards.add((
			lottery_id = lottery_id,
			ticket_id = ticket_id,
			reward = app.map_reward(reward),
			timestamp = timestamp
		));
	}
	
	return (
		master_address = status.master_address,
		lottery = status.lottery,
		tickets = status.tickets,
		wallets = status.wallets,
		draw_time = status.draw_time,
		next_snapshot_time = status.next_snapshot_time,
		rewards = rewards
	).to_gtv_pretty();
}

query get_lottery_state(lottery_id: integer) {
	return app.Lottery(lottery_id).state;
}

query get_wallet_lottery_state_for_round(address: text, lottery_id: integer) {
	return app.get_wallet_status(address, app.Lottery(lottery_id))?.to_gtv_pretty();
}

query get_linked_wallets(address: text) {
	return app.get_linked_wallets(address);
}

query is_master_wallet(address: text) {
	return app.wallet @? { .master_address == address, .address == address }??;
}

query get_master_wallet(address: text) {
	return app.Wallet(address).master_address;
}

query get_wallets_page(page_size: integer, last_rowid: integer) {
	return app.get_wallets_page(page_size, last_rowid);
}

query get_active_lottery_id() {
	return app.get_active_lottery()?.id;
}

query get_active_lottery_details() {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.lottery_to_gtv_pretty(lottery);
}

query get_active_lottery_winners_page(page_size: integer, after_rowid: integer) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_lottery_winners_page(lottery, page_size, after_rowid);
}

query get_lottery_winners_page(lottery_id: integer, page_size: integer, after_rowid: integer) {
	return app.get_lottery_winners_page(app.Lottery(lottery_id), page_size, after_rowid);
}

query estimate_ticket_count_for_active_lottery(balances: list&lt;app.token_balance&gt;) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.estimate_ticket_count(lottery, balances);
}

query get_active_lottery_wallet_tickets(address: text) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_wallet_tickets(lottery, app.Wallet(address));	
}

query get_active_lottery_tickets() {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_lottery_tickets(lottery);	
}

query get_registered_tokens() {
	return app.get_registered_tokens();
}

query get_wallet_rewards(address: text) {
	val rewards = app.get_wallet_rewards(app.Wallet(address));
	val response = list&lt;(
		lottery_id: integer, 
		ticket_id: integer, 
		reward: map&lt;text, gtv&gt;, 
		timestamp: integer
	)&gt;();
	
	for ((lottery_id, ticket_id, reward, timestamp) in rewards) {
		response.add((
			lottery_id = lottery_id,
			ticket_id = ticket_id,
			reward = app.map_reward(reward),
			timestamp = timestamp
		));
	}
	
	return response;
}

query get_lottery_rewards_page_for_wallet_desc(
	address: text, 
	page_size: integer, 
	before_lottery_id: integer
) {
	val page = app.get_lottery_rewards_page_for_wallet_desc(
		app.Wallet(address), 
		page_size, 
		before_lottery_id
	);
	
	val mapped_lotteries = list&lt;(lottery_id: integer, rewards: list&lt;map&lt;text, gtv&gt;&gt;)&gt;();
	for ((lottery_id, rewards) in page.lotteries) {
		val mapped_rewards = list&lt;map&lt;text, gtv&gt;&gt;();
		for (reward in rewards) {
			mapped_rewards.add(app.map_reward(reward));
		}
		mapped_lotteries.add((
			lottery_id = lottery_id,
			rewards = mapped_rewards
		));
	}
	
	return (
		count = page.count,
		lotteries = mapped_lotteries,
		oldest_lottery_id = page.oldest_lottery_id
	);
}
</string>
                            </entry>
                            <entry key="app/rand.rell">
                                <string>object random_pool { 
	mutable state: byte_array = chain_context.blockchain_rid;
}

function random(): byte_array {
	val new_state = (
		random_pool.state, 
		op_context.transaction.tx_hash,
		block @ { .block_height == op_context.block_height - 1} .block_rid
	).hash(); 
    random_pool.state = new_state;
    return new_state;
}

function random_hex(): text {
	return random().to_hex();
}

function random_integer() = integer.from_hex(random().to_hex().sub(0, 14));</string>
                            </entry>
                            <entry key="app/rewards.rell">
                                <string>
entity reward {
	index type: reward_type;
	details: byte_array;
}

enum reward_type {
	plot
}

struct reward_details {
	type: details_type;
	value: gtv;
}

enum details_type {
	id,
	range_id,
	empty
}

function reward_id_details(id: byte_array) = reward_details(
	details_type.id, id.to_gtv()
).to_bytes();

function reward_range_id_details(id: integer) = reward_details(
	details_type.range_id, id.to_gtv()
).to_bytes();

function reward_empty_details() = reward_details(
	details_type.empty, x"".to_gtv()
).to_bytes();

enum reward_spec_type {
	range,
	lst,
	count
}

struct reward_range {
	start: integer;
	end: integer;
}

struct _reward_count {
	value: integer;
}

struct reward_list {
	rewards: list&lt;byte_array&gt;;
}

function plot_list(plot_ids: list&lt;byte_array&gt;): reward_spec {
	return reward_spec(
		reward_type.plot,
		reward_spec_type.lst,
		reward_list(plot_ids).to_gtv()
	);
}

function plot_count(count: integer): reward_spec {
	return reward_spec(
		reward_type.plot,
		reward_spec_type.count,
		_reward_count(count).to_gtv()		
	);
}

function plot_range(start: integer, end: integer): reward_spec {
	return reward_spec(
		reward_type.plot,
		reward_spec_type.range,
		reward_range(start, end).to_gtv()
	);
}

function reward_count_from_bytes(reward_spec: byte_array): _reward_count {
	return _reward_count.from_gtv(gtv.from_bytes(reward_spec));
}

function reward_list_from_bytes(reward_spec: byte_array): reward_list {
	return reward_list.from_gtv(gtv.from_bytes(reward_spec));
}

function reward_range_from_bytes(reward_spec: byte_array): reward_range {
	return reward_range.from_gtv(gtv.from_bytes(reward_spec));
}

struct reward_spec {
	reward_type;
	reward_spec_type;
	details: gtv; // TODO: rename
}

function rewards_from_bytes(rewards: byte_array): list&lt;reward_spec&gt; {
	return list&lt;reward_spec&gt;.from_gtv(gtv.from_bytes(rewards));
}

function reward_count(lottery): integer {
	return 0;
}

function validate_rewards(rewards: list&lt;reward_spec&gt;) {
	for (reward in rewards) {
		when (reward.reward_spec_type) {
			reward_spec_type.range -&gt; {
				val range = reward_range.from_gtv(reward.details);
				require(range.start &gt;= 0);
				require(range.start &lt; range.end);	
			}
			reward_spec_type.count -&gt; {
				val count = _reward_count.from_gtv(reward.details);
				require(count.value &gt; 0);
			}
			reward_spec_type.lst -&gt; {
				val rewards_list = reward_list.from_gtv(reward.details);
				require(rewards_list.rewards.size() &gt; 0);
			}
		}
	}
}

function map_reward(reward): map&lt;name, gtv&gt; {
	val reward_type = reward.type;
	val reward_map = [
		"type": reward_type.name.to_gtv()
	];
	
	val reward_details = reward_details.from_bytes(reward.details);
	val details_map = [
		"type": reward_details.type.name.to_gtv()
	];
	
	when (reward_details.type) {
		details_type.id,
		details_type.range_id -&gt; 
			details_map["value"] = reward_details.value;
	}
	
	reward_map["details"] = details_map.to_gtv();
	
	return reward_map;
}

function get_lottery_rewards_page_for_wallet_desc(wallet, page_size: integer, before_lottery_id: integer) {
	val lottery_list = list&lt;(lottery_id: integer, rewards: list&lt;reward&gt;)&gt;();
	
	val lotteries = lottery @* { .id &lt; before_lottery_id } ( @omit @sort_desc .id, lottery ) limit page_size;
	
	for (lottery in lotteries) {
		lottery_list.add((
			lottery_id = lottery.id,
			rewards = winner @* { lottery, wallet } ( .reward )
		));
	}
	
	val oldest_lottery_id = if (lottery_list.size() &gt; 0) 
			lottery_list[lottery_list.size()-1].lottery_id 
		else 
			0;

	return (
		count = lottery_list.size(),
		lotteries = lottery_list,
		oldest_lottery_id = oldest_lottery_id
	);
}
</string>
                            </entry>
                            <entry key="app/snapshot.rell">
                                <string>
// Add snapshot time?
entity snapshot {
	key lottery, wallet, round: snapshot_round, token;
	amount: integer;
	mutable is_processed: boolean;
}

entity snapshot_round {
	key lottery, id: integer;
	time: timestamp;
}

entity token {
	key name, chain: text; // Is chain needed?
}

function Token(name, chain: text) = require(
	token @? { .name == name, .chain == chain },
	"Token not found: " + name
);

struct token_balance {
	token_name: name;
	chain: name;
	amount: integer;
}

function snapshot_rounds_count(lottery) =
	snapshot_round @? { lottery } ( @omit @group .lottery, @sum 1) ?: 0;

function create_new_snapshotting_round(lottery, time: timestamp) {
	require(
		lottery.state == lottery_state.created 
			or 
		lottery.state == lottery_state.snapshotting_finished,
		"Cannot create new snapshot round while lottery is in state: " + lottery.state
	);
	
	create snapshot_round(
		lottery,
		id = snapshot_rounds_count(lottery) + 1,
		time
	);	
}
	
function current_snapshot_round(lottery) =
	snapshot_round @? { lottery } (@omit @sort_desc .id, snapshot_round) limit 1;

function save_wallet_snapshot(lottery, wallet, balances: list&lt;token_balance&gt;) {
	require(
		lottery.state == lottery_state.snapshotting,
		"Cannot save the snapshot. Lottery is not in snapshotting state"
	);
	
	val round = require(
		current_snapshot_round(lottery),
		"Cannot find current snapshotting round"		
	);
	
	for (balance in balances) {
		create snapshot(
			lottery,
			wallet,
			Token(balance.token_name, balance.chain),
			round,
			amount = balance.amount,
			false
		);
	}
}
</string>
                            </entry>
                            <entry key="app/ticket.rell">
                                <string>

struct ticket_rule {
	token_name: text;
	token_chain: text;
	token_amount: integer;
	ticket_amount: integer;
}

struct _ticket_rule {
	token;
	token_amount: integer;
	ticket_amount: integer;
}

entity ticket_range {
	key lottery, snapshot;
	index start: integer, end: integer;
	index wallet;
}

function TicketRange(lottery, ticket_id: integer) = require(
	ticket_range @? { lottery, .start &lt;= ticket_id, .end &gt;= ticket_id },
	"No ticket range in lottery '" + lottery.id + "', that includes ticket: " + ticket_id
);

function ticket_rules_from_bytes(rules: byte_array) = 
	list&lt;ticket_rule&gt;.from_gtv(gtv.from_bytes(rules));

function _ticket_rules_from_bytes(rules: byte_array): list&lt;_ticket_rule&gt; {
	val deserialized_rules = ticket_rules_from_bytes(rules);
	val _rules = list&lt;_ticket_rule&gt;();
	for (rule in deserialized_rules) {
		_rules.add(_ticket_rule(
			Token(rule.token_name, rule.token_chain),
			token_amount = rule.token_amount,
			ticket_amount = rule.ticket_amount
		));
	}
	return _rules;
}

function token_ticket_rules_map(rules: list&lt;_ticket_rule&gt;): map&lt;token, _ticket_rule&gt; {
	 val rules_map = map&lt;token, _ticket_rule&gt;();
	 for (rule in rules) {
	 	rules_map[rule.token] = rule;
	 }
	 return rules_map;
}

function ticket_count_for_snapshot(snapshot, rules: map&lt;token, _ticket_rule&gt;): integer {
	val ticket_rule = rules[snapshot.token];
	val ticket_multipler = snapshot.amount / ticket_rule.token_amount;
	return ticket_multipler * ticket_rule.ticket_amount;
}

function ticket_count_for_token_balance(token, amount: integer, rules: map&lt;token, _ticket_rule&gt;): integer {
	val ticket_rule = rules[token];
	val ticket_multiplier = amount / ticket_rule.token_amount;
	return ticket_multiplier * ticket_rule.ticket_amount;
}


function validate_ticket_rules(rules: list&lt;ticket_rule&gt;) {
	val tokens = set&lt;token&gt;();
	
	for (rule in rules) {
		val token = Token(rule.token_name, rule.token_chain);
		
		require(
			not tokens.contains(token),
			"More than one rule defined for token '" + rule.token_name + "'"
		);
		
		tokens.add(token);
			
		require(
			rule.token_amount &gt; 0, 
			"Token amount has to be greater than 0"
		);
		require(
			rule.ticket_amount &gt; 0, 
			"Ticket amount has to be greater than 0"
		);
	}
}

function get_ticket_count(lottery) =
	ticket_range @? { lottery } ( 
		@omit @group .lottery, 
		@sum .end - .start + 1
	) ?: 0;

function get_wallet_ticket_count(lottery, wallet) =
	ticket_range @? { lottery, wallet } ( 
		@omit @group .lottery,  
		@sum .end - .start + 1
	) ?: 0;

function get_wallet_tickets(lottery, wallet) =
	ticket_range @* { lottery, wallet } (.start, .end);
	
function get_lottery_tickets(lottery) = 
	ticket_range @* { lottery } (
		.start, 
		.end, 
		address = .wallet.address
	);

function issue_tickets(lottery) {
	val snapshots = snapshot @* { 
		lottery, 
		.is_processed == false
	};
	
	val participation_rules = _participation_rules_from_bytes(lottery.participation_rules);
	val ticket_rules = _ticket_rules_from_bytes(lottery.ticket_rules);
	val rules_map = token_ticket_rules_map(ticket_rules);
	
	var ticket_count = get_ticket_count(lottery);	
	val master_wallet_eligibility = map&lt;(wallet, snapshot_round), boolean&gt;();
	
	for (snapshot in snapshots) {
		val wallet = snapshot.wallet;
		val master_wallet = if (wallet.master_address != wallet.address) 
			MasterWallet(wallet.master_address) 
		else 
			wallet;
			
		if (is_master_wallet_eligible_for_tickets(master_wallet, snapshot, participation_rules, master_wallet_eligibility)) {
			val ticket_count_to_issue = ticket_count_for_snapshot(snapshot, rules_map);
			
			create ticket_range(
				lottery, 
				snapshot,
				wallet = master_wallet,
				start = ticket_count,
				end = ticket_count + ticket_count_to_issue - 1
			);
			
			ticket_count += ticket_count_to_issue;
		}
		snapshot.is_processed = true;
	}
}

function is_balance_satisfying_participation_rules(token, amount: integer, rules: map&lt;token, integer&gt;) =
	if (token in rules) amount &gt;= rules[token] else false;

function estimate_ticket_count(lottery, balances: list&lt;token_balance&gt;) {
	val participation_rules_map = _participation_rules_map_from_bytes(lottery.participation_rules);
	
	val ticket_rules = _ticket_rules_from_bytes(lottery.ticket_rules);
	val rules_map = token_ticket_rules_map(ticket_rules);
	
	var ticket_count = 0;
	var can_participate = false;
	
	for (balance in balances) {
		val token = Token(balance.token_name, balance.chain);
		ticket_count += ticket_count_for_token_balance(token, balance.amount, rules_map);
		if (is_balance_satisfying_participation_rules(token, balance.amount, participation_rules_map)) {
			can_participate = true;
		}
	}
	
	return if (can_participate) ticket_count else 0;
}

function is_master_wallet_eligible_for_tickets(
	master_wallet: wallet, 
	wallet_snapshot: snapshot, 
	rules: list&lt;_participation_rule&gt;,
	wallets_eligibility: map&lt;(wallet, snapshot_round), boolean&gt;
): boolean {
	if (wallets_eligibility.contains((master_wallet, wallet_snapshot.round))) return wallets_eligibility[(master_wallet, wallet_snapshot.round)];
	
	// TODO: Improve this block. Add support to combine rules with AND and OR operator.
	// At the moment wallet is eligible for tickets only if all rules are satisfied.
	for (rule in rules) {
		val master_wallet_snapshot = 
			if (wallet_snapshot.wallet == master_wallet and rule.token == wallet_snapshot.token) 
				wallet_snapshot 
			else
				snapshot @? {
					wallet_snapshot.lottery,
					master_wallet,
					wallet_snapshot.round,
					rule.token
				};
			
		if (empty(master_wallet_snapshot)) {
			wallets_eligibility[(master_wallet, wallet_snapshot.round)] = false;
			return false;
		}
		
		if (master_wallet_snapshot.amount &lt; rule.amount) {
			wallets_eligibility[(master_wallet, wallet_snapshot.round)] = false;
			return false;
		}
	}
	
	wallets_eligibility[(master_wallet, wallet_snapshot.round)] = true;
	return true;
}
</string>
                            </entry>
                            <entry key="app/token.rell">
                                <string>
function get_registered_tokens() =  
	token @* {} ( .name, .chain );</string>
                            </entry>
                            <entry key="app/wallet.rell">
                                <string>
entity wallet {
	mutable master_address: text;
	key address: text;
	key master_address, address;
}

function Wallet(address: text) = require(
	wallet @? { .address == address },
	"Wallet not found: " + address	
);

function MasterWallet(address: text): wallet {
	val wallet = Wallet(address);
	
	require(
		wallet.address == wallet.master_address,
		"Wallet is not master wallet: " + address
	);
	
	return wallet;
}

function register_master_wallet(address: text): wallet {
	require (
		empty(wallet @? { address }),
		"Wallet already exits: " + address
	);
	
	return create wallet(master_address = address, address = address);
}

function link_wallet(address: text, master_address: text): wallet {
	// It would be better to throw exception
	// and use different operation for registering master wallet
	if (address == master_address) {
		return register_master_wallet(address);
	}
	
	val master_wallet = MasterWallet(master_address);
	
	val wallet = wallet @? { address };
	
	if (empty(wallet)) {
		return create wallet(master_address, address);
	} else {
		require(
			wallet.master_address == wallet.address,
			"Unlink wallet before linking it to a different master wallet"
		);
		
		require(
			(wallet @* { .master_address == address } (.rowid)).size() == 0,
			"Wallet cannot be linked while it has child wallets"
		);
		
		wallet.master_address = master_address;
		return wallet;
	}
}

function unlink_wallet(wallet) {
	wallet.master_address = wallet.address;
}

function get_wallet_status(address: text, lottery?) {
	val master_address = wallet @? { .address == address }.master_address;
	
	if (empty(master_address)) return null;
	
	val master_wallet = MasterWallet(master_address);
	
	val tickets = (
		issued = if (lottery??) get_ticket_count(lottery) else null,
		owned = if (lottery??) get_wallet_ticket_count(lottery, master_wallet) else null
	);
		
	return (
		master_address = master_address,
		lottery = get_wallet_lottery_ticket_info(master_wallet, lottery),
		tickets = tickets,
		wallets = get_linked_wallets(master_address),
		draw_time = lottery?.draw_time,
		next_snapshot_time = if (lottery??) current_snapshot_round(lottery)?.time else null,
		rewards = get_wallet_rewards(master_wallet)
	);
}

function get_linked_wallets(address: text) =
	wallet @* { 
		.master_address == address, 
		.address != address
	} ( .address );

function get_wallet_rewards(wallet) = 
	winner @* { wallet } (
		lottery_id = .lottery.id,
		ticket_id = .ticket_id,
		reward = .reward,
		timestamp = .lottery.draw_time
	);
	
function is_eligible_for_reward(lottery, wallet): boolean {
	require(
		wallet.master_address == wallet.address,
		"Reward can be assigned only to master wallet"
	);
	
	// Wallet already got a reward
	if (winner @? { lottery, wallet }??) return false;
	
	val snapshot_round = require(
		current_snapshot_round(lottery),
		"Error: Cannot find last snapshot round"
	);
	
	// Check if at least one ticket range is assigned 
	// to wallet in the last snapshotting round
	val last_snapshot_ticket_range = (
		tr: ticket_range, 
		s: snapshot
	) @? {
		s.lottery == lottery,
		s.wallet == wallet,
		s.round == snapshot_round,
		tr.lottery == lottery,
		tr.snapshot == s
	} limit 1;

	// User withdrew deposited tokens before the last round
	if(empty(last_snapshot_ticket_range)) return false; 

	return true;
}

function get_wallets_page(page_size: integer, after_rowid: integer) {
	val id = rowid.from_gtv(after_rowid.to_gtv());
	val wallets = wallet @* { .rowid &gt; id } (
		@omit @sort .rowid, 
		wallet
	) limit page_size;
	
	val last_rowid = if (wallets.size() &gt; 0) 
			integer.from_gtv(wallets[wallets.size()-1].rowid.to_gtv()) 
		else 
			0;
			
	val result = list&lt;(master_address: text, address: text)&gt;();
	for (wallet in wallets) {
		result.add((
			master_address = wallet.master_address,
			address = wallet.address
		));
	}
	
	return (
		count = wallets.size(),
		wallets = result,
		last_rowid = last_rowid
	);
}

function get_wallet_lottery_ticket_info(wallet, lottery?) {
	if (empty(lottery)) return null;
	
	return (
		id = lottery.id,
		state = lottery.state,
		draw_time = lottery.draw_time,
		next_snapshot_time = current_snapshot_round(lottery)?.time ?: 0,
		tickets = (
			issued = get_ticket_count(lottery),
			owned = get_wallet_ticket_count(lottery, wallet)
		)
	);
}
</string>
                            </entry>
                        </dict>
                    </entry>
                </dict>
            </entry>
        </dict>
    </entry>
    <entry key="signers">
        <array>
            <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
        </array>
    </entry>
</dict>
