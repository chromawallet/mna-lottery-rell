const Token = require("../model/token");
const { Postchain, KeyPair, op, nop } = require("ft3-lib");

module.exports = class LotteryService {
  static async initialize(url, brid, adminPrivKey) {
    const blockchain = await new Postchain(url).blockchain(brid);
    const adminKeyPair = new KeyPair(adminPrivKey);

    return new LotteryService(blockchain, adminKeyPair);
  }

  constructor(blockchain, adminKeyPair) {
    this.blockchain = blockchain;
    this.adminKeyPair = adminKeyPair;
  }

  async getWalletLotteryStatus(address, round) {
    return await this.blockchain.query("get_wallet_lottery_state_for_round", {
      address,
      round
    });
  }

  async getActiveLotteryWalletStatus(address) {
    return await this.blockchain.query("get_wallet_lottery_state", { address });
  }

  async getActiveLotteryId() {
    return await this.blockchain.query("get_active_lottery_id", {});
  }

  async getLotteryWinners(lotteryId, pageSize, lastRowid) {
    return await this.blockchain.query("get_lottery_winners_page", {
      lottery_id: lotteryId,
      page_size: pageSize,
      after_rowid: lastRowid
    });
  }

  async registerMasterWallet(address) {
    return await this.blockchain.transactionBuilder()
      .add(op("register_master_wallet", address))
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post()
  }

  async linkWallet(masterAddress, address) {
    return await this.blockchain.transactionBuilder()
      .add(op("link_wallet", address, masterAddress)) // TODO: Pass timestamp and remove nop
      .add(nop()) // We need `nop` because user might unlink wallet
      // and then re-link it to the same master
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async getLinkedWallets(address) {
    return await this.blockchain.query("get_linked_wallets", { address });
  }

  async getMasterWallet(address) {
    return await this.blockchain.query("get_master_wallet", { address });
  }

  async isMasterWallet(address) {
    return await this.blockchain.query("is_master_wallet", { address });
  }

  async createLottery(participationRules, ticketRules, rewards, description, drawTime, typeDetails, evmDetails) {
    return await this.blockchain.transactionBuilder()
      .add(op("create_lottery", participationRules, ticketRules, rewards, description, drawTime, typeDetails, evmDetails))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async updateLottery(participationRules, ticketRules, rewards, description, drawTime) {
    return await this.blockchain.transactionBuilder()
      .add(op("update_lottery", participationRules, ticketRules, rewards, description, drawTime))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async updateLotteryEVMDetails(lotteryId, evmDetails) {
    return await this.blockchain.transactionBuilder()
      .add(op("update_lottery_evm_details", lotteryId, evmDetails))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async getActiveLotteryDetails() {
    return await this.blockchain.query("get_active_lottery_details", {});
  }

  async registerTokens(tokens) {
    return await this.blockchain.transactionBuilder()
      .add(op("register_tokens", tokens))
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async getRegisteredTokens() {
    const response = await this.blockchain.query("get_registered_tokens", {});
    return response.map(({name, chain}) => new Token(name, chain));
  }

  async createNewSnapshottingRound(time) {
    return await this.blockchain.transactionBuilder()
      .add(op("create_new_snapshotting_round", time))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async saveWalletSnapshot(address, balances) {
    return await this.blockchain.transactionBuilder()
      .add(op("save_wallet_snapshot", address, balances))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async issueTickets() {
    return await this.blockchain.transactionBuilder()
      .add(op("issue_tickets"))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async setLotteryState(state) {
    return await this.blockchain.transactionBuilder()
      .add(op("set_lottery_state", state))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async draw() {
    return await this.blockchain.transactionBuilder()
      .add(op("draw"))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }

  async getActiveLotteryWalletTickets(address) {
    return await this.blockchain.query("get_active_lottery_wallet_tickets", {
      address
    });
  }

  async getActiveLotteryTickets() {
    return await this.blockchain.query("get_active_lottery_tickets", {});
  }

  async estimateTicketCount(balances) {
    return await this.blockchain.query("estimate_ticket_count_for_active_lottery", {
      balances
    });
  }

  async getWalletRewards(address) {
    return await this.blockchain.query("get_wallet_rewards", { address });
  }

  async getLotteryRewardsPageForWallet(address, pageSize, beforeLotteryId) {
    return await this.blockchain.query("get_lottery_rewards_page_for_wallet_desc", {
      address,
      page_size: pageSize,
      before_lottery_id: beforeLotteryId
    })
  }

  async saveSnapshotStatsForAllLotteries() {
    return await this.blockchain.transactionBuilder()
      .add(op("save_snapshot_stats_for_all_lotteries"))
      .add(nop())
      .build([this.adminKeyPair.pubKey])
      .sign(this.adminKeyPair)
      .post();
  }
}

