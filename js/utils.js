
const http = require('http');

async function getBlockchainBrid(url, iid) {
  return new Promise((resolve, reject) => {
    http.get(`${url}/brid/iid_${iid}`, response => {
      response.on('data', brid => {
        resolve(brid.toString());
      })
    }).on('error', error => {
      reject(error);
    });
  });
}

module.exports = {
  getBlockchainBrid
}