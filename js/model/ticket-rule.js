
module.exports = class TicketRule {
  constructor(token, chain, tokenAmount, ticketAmount) {
    this.token = token;
    this.chain = chain;
    this.tokenAmount = tokenAmount;
    this.ticketAmount = ticketAmount;
  }

  toGTV() {
    return [this.token, this.chain, this.tokenAmount, this.ticketAmount];
  }

  toString() {
    const ticketString = this.ticketAmount === 1 ? "ticket" : "tickets";
    const tokenString = this.tokenAmount === 1 ? "token" : "tokens";
    return `${this.ticketAmount} ${ticketString} for ${this.tokenAmount} ${this.token}/${this.chain} ${tokenString}`
  }
}
