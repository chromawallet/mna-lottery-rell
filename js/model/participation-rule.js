
module.exports = class ParticipationRule {
  constructor(token, chain, minAmount) {
    this.token = token;
    this.chain = chain;
    this.minAmount = minAmount;
  }

  toGTV() {
    return [this.token, this.chain, this.minAmount];
  }
}