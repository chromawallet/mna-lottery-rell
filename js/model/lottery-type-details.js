
module.exports = class LotteryTypeDetails {
  constructor(type, details = null) {
    this.type = type;
    this.details = details;
  }

  toGTV() {
    return [this.type, this.details];
  }
}
