
module.exports = class Snapshot {
  constructor(token, chain, amount) {
    this.token = token;
    this.chain = chain;
    this.amount = amount;
  }

  toGTV() {
    return [this.token, this.chain, this.amount];
  }
}
