
module.exports = {
  verbose: true,
  roots: [
    "<rootDir>/tests"
  ],
  setupFilesAfterEnv: [
    "<rootDir>/jest.setup.js"
  ],
  // globalSetup: "<rootDir>/tests/setup.js"
}