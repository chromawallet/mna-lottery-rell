const LotteryService = require("../js/service/lottery-service");
const LotteryState = require("../js/model/lottery-state");
const TicketRule = require("../js/model/ticket-rule");
const Token = require("../js/model/token");
const TokenBalance = require("../js/model/token-balance");
const ParticipationRule = require("../js/model/participation-rule");

const { getBlockchainBrid } = require("../js/utils");
const { PlotRange } = require("../js/model/reward-spec");
const { util } = require("postchain-client");

require("dotenv").config();

const url = "http://localhost:7740";

(async () => {
  const brid = await getBlockchainBrid(url, 0);
  const service = await LotteryService.initialize(url, brid, process.env.ADMIN_PRIV_KEY);

  await service.registerTokens([
    new Token("CHR", "ETH"),
    new Token("ALICE", "BSC")
  ]);

  await service.createLottery(
    [
      new ParticipationRule("ALICE", "BSC", 20)
    ],
    [
      new TicketRule("CHR", "ETH", 20, 1),
      new TicketRule("ALICE", "BSC", 1, 1)
    ],
    [
      new PlotRange(0, 1000)
    ],
    "Description",
    1621008000000
  );

  const lotteryId = await service.getActiveLotteryId();

  await service.createNewSnapshottingRound(1621008000000)
  await service.setLotteryState(LotteryState.Snapshotting);

  for (let i=0; i<1000; i++) {
    const address = util.randomBytes(32).toString('hex');
    await service.registerMasterWallet(address);
    const snapshotSaveStart = Date.now();
    await service.saveWalletSnapshot(
      address, 
      [
        new TokenBalance("ALICE", "BSC", 1000)
      ]
    );
    console.log("------------------------------------------------------")
    console.log(`Generated ${i} ${Date.now() - snapshotSaveStart}`);
  }
  const issueTicketsStart = Date.now();
  await service.issueTickets();
  console.log(`Tickets issued: ${Date.now() - issueTicketsStart}`);

  await service.setLotteryState(LotteryState.WaitingDraw);
  const start = Date.now();
  await service.draw();
  console.log(`Duration ${Date.now() - start}`);

  console.log(JSON.stringify(await service.getLotteryWinners(lotteryId, 24, 0)));
})()