const LotteryService = require("../js/service/lottery-service");
const Token = require("../js/model/token");
const TokenBalance = require("../js/model/token-balance");
const LotteryState = require("../js/model/lottery-state");

const { getBlockchainBrid } = require("../js/utils");
const { createTestLottery, randomAddress } = require("./utils");

require("dotenv").config();

const url = "http://localhost:7740";

let brid = null;
let lotteryService = null;

describe("Ticket", () => {
  beforeAll(async () => {
    brid = await getBlockchainBrid(url, 0);
    lotteryService = await LotteryService.initialize(url, brid, process.env.ADMIN_PRIV_KEY);
  })

  it("ranges do not overlap", async () => {
    await createTestLottery(lotteryService);

    const address1 = randomAddress();
    const address2 = randomAddress();
    await lotteryService.registerMasterWallet(address1);
    await lotteryService.registerMasterWallet(address2);

    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);

    await lotteryService.saveWalletSnapshot(address1, [
        new TokenBalance("ALICE", "BSC", 1000)
    ]);

    await lotteryService.saveWalletSnapshot(address2, [
      new TokenBalance("ALICE", "BSC", 1000)
    ]);

    await lotteryService.issueTickets();

    const tickets1 = await lotteryService.getActiveLotteryWalletTickets(address1);
    const tickets2 = await lotteryService.getActiveLotteryWalletTickets(address2);
    const tickets = await lotteryService.getActiveLotteryTickets();

    expect(tickets1).toEqual([{ start: 0, end: 999 }]);
    expect(tickets2).toEqual([{ start: 1000, end: 1999 }]);
    expect(tickets).toEqual([
      { address: address1, start: 0, end: 999 },
      { address: address2, start: 1000, end: 1999 }
    ]);

    await lotteryService.setLotteryState(LotteryState.Canceled);
  })

  it("should estimate ticket count", async () => {
    await createTestLottery(lotteryService);

    const balances = [new TokenBalance("ALICE", "BSC", 100)]
    await expect(lotteryService.estimateTicketCount(balances)).resolves.toBe(100);

    await lotteryService.setLotteryState(LotteryState.Canceled);
  })
})