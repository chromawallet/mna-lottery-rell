
require("dotenv").config();

const LotteryService = require("../js/service/lottery-service");
const Token = require("../js/model/token");
const { getBlockchainBrid } = require("../js/utils");

(async () => {
  const url = "http://localhost:7740";
  const brid = await getBlockchainBrid(url, 0);
  const lotteryService = await LotteryService.initialize(url, brid, process.env.ADMIN_PRIV_KEY);
  await lotteryService.registerTokens([
    new Token("ALICE", "BSC"),
    new Token("CHR", "BSC")
  ]);
})()
