
entity wallet {
	mutable master_address: text;
	key address: text;
	key master_address, address;
}

function Wallet(address: text) = require(
	wallet @? { .address == address },
	"Wallet not found: " + address	
);

function MasterWallet(address: text): wallet {
	val wallet = Wallet(address);
	
	require(
		wallet.address == wallet.master_address,
		"Wallet is not master wallet: " + address
	);
	
	return wallet;
}

function register_master_wallet(address: text): wallet {
	require (
		empty(wallet @? { address }),
		"Wallet already exits: " + address
	);
	
	return create wallet(master_address = address, address = address);
}

function link_wallet(address: text, master_address: text): wallet {
	// It would be better to throw exception
	// and use different operation for registering master wallet
	if (address == master_address) {
		return register_master_wallet(address);
	}
	
	val master_wallet = MasterWallet(master_address);
	
	val wallet = wallet @? { address };
	
	if (empty(wallet)) {
		return create wallet(master_address, address);
	} else {
		require(
			wallet.master_address == wallet.address,
			"Unlink wallet before linking it to a different master wallet"
		);
		
		require(
			(wallet @* { .master_address == address } (.rowid)).size() == 0,
			"Wallet cannot be linked while it has child wallets"
		);
		
		wallet.master_address = master_address;
		return wallet;
	}
}

function unlink_wallet(wallet) {
	wallet.master_address = wallet.address;
}

function delete_wallet(w: wallet) {
    // Deletes wallet together with linked wallets, 
    // but only if wallet was not snapshotted before
    if (w.master_address == w.address) {
        delete wallet @* { .master_address == w.address };
    }
    
    delete w;
}

function get_wallet_status(address: text, lottery?) {
	val master_address = wallet @? { .address == address }.master_address;
	
	if (empty(master_address)) return null;
	
	val master_wallet = MasterWallet(master_address);
		
	return (
		master_address = master_address,
		lottery = get_wallet_lottery_ticket_info(master_wallet, lottery),
		wallets = get_linked_wallets(master_address),
		rewards = get_wallet_rewards(master_wallet)
	);
}

function get_linked_wallets(address: text) =
	wallet @* { 
		.master_address == address, 
		.address != address
	} ( .address );

function get_wallet_rewards(wallet) {
	val rewards = winner @* { wallet } (
		@sort lottery_id = .lottery.id,
		ticket_id = .ticket_id,
		reward = .reward,
		timestamp = .lottery.draw_time
	);
	
	val evm_details = (w: winner, evm: evm_lottery_details) @* {
		w.wallet == wallet,
		w.lottery == evm.lottery
	} (
		@sort lottery_id = w.lottery.id,
		evm = evm
	);

	val result = list<(
		lottery_id: integer,
		ticket_id: integer,
		reward: reward,
		timestamp: integer,
		evm_details: evm_details?
	)>();
	
	var evm_index = 0;
	for ((lottery_id, ticket_id, reward, timestamp) in rewards) {
		var evm: evm_lottery_details? = null;

		if (evm_details.size() > evm_index and evm_details[evm_index].lottery_id == lottery_id) {
			evm = evm_details[evm_index].evm;
			evm_index++;
		}
		
		result.add((
			lottery_id = lottery_id,
			ticket_id = ticket_id,
			reward = reward,
			timestamp = timestamp,
			evm_details = if (empty(evm)) null else evm_details(
				lottery_id = evm.lottery_id, 
				chain = evm.chain, 
				deposit_contract = evm.deposit_contract
			)
		));
	}
	
	return result;
}
	
function get_wallet_rewards_long(wallet) {
	val rewards = (w: winner, tr: ticket_range) @* {
		w.wallet == wallet,
		tr.lottery == w.lottery,
		tr.wallet == w.wallet
	} (
		@group lottery_id = w.lottery.id,
		@group ticket_id = w.ticket_id,
		@group reward = w.reward,
		@group draw_time = w.lottery.draw_time,
		@sum wallet_ticket_count = tr.end - tr.start + 1
	);
	
	val mapped_rewards = list<(
		lottery_id: integer,
		ticket_id: integer,
		reward: map<name, gtv>,
		draw_time: integer,
		wallet_ticket_count: integer
	)>();
	
	for (reward in rewards) {
		mapped_rewards.add((
			lottery_id = reward.lottery_id,
			ticket_id = reward.ticket_id,
			reward = map_reward(reward.reward),
			draw_time = reward.draw_time,
			wallet_ticket_count = reward.wallet_ticket_count 
		));
	}
	
	return mapped_rewards;
}

/*
 * Since we can have non-snapshot rounds (e.g. used to issue bonus tickets) we have to pass last snapshot round
 */	
function is_eligible_for_reward(lottery, wallet, last_snapshot_round: snapshot_round): boolean {
	require(
		wallet.master_address == wallet.address,
		"Reward can be assigned only to master wallet"
	);
	
	// Wallet already got a reward
	if (winner @? { lottery, wallet }??) return false;
	
	// Check if at least one ticket range is assigned 
	// to wallet in the last snapshotting round
	val last_snapshot_ticket_range = (
		tr: ticket_range, 
		s: snapshot
	) @? {
		s.lottery == lottery,
		s.wallet == wallet,
		s.round == last_snapshot_round,
		tr.lottery == lottery,
		tr.snapshot == s
	} limit 1;

	// User withdrew deposited tokens before the last round
	if(empty(last_snapshot_ticket_range)) return false; 

	return true;
}

function get_wallets_page(page_size: integer, after_rowid: integer) {
	val id = rowid.from_gtv(after_rowid.to_gtv());
	val wallets = wallet @* { .rowid > id } (
		@omit @sort .rowid, 
		wallet
	) limit page_size;
	
	val last_rowid = if (wallets.size() > 0) 
			integer.from_gtv(wallets[wallets.size()-1].rowid.to_gtv()) 
		else 
			0;
			
	val result = list<(master_address: text, address: text)>();
	for (wallet in wallets) {
		result.add((
			master_address = wallet.master_address,
			address = wallet.address
		));
	}
	
	return (
		count = wallets.size(),
		wallets = result,
		last_rowid = last_rowid
	);
}

function get_wallet_lottery_ticket_info(wallet, lottery?) {
	if (empty(lottery)) return null;
	
	return (
		id = lottery.id,
		state = lottery.state,
		draw_time = lottery.draw_time,
		next_snapshot_time = current_snapshot_round(lottery)?.time ?: 0,
		tickets = (
			issued = get_ticket_count(lottery),
			owned = get_wallet_ticket_count(lottery, wallet)
		),
		snapshot_stats = get_lottery_snapshot_stats(lottery),
		type = get_lottery_type_details(lottery)
	);
}
