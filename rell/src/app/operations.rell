module;

import app;

function get_active_lottery() =
	require(app.get_active_lottery(), "No active lottery");

operation register_token(name, chain: name) {
	app.require_admin();
	create app.token(name, chain);
}

operation register_tokens(tokens: list<(name, name)>) {
	app.require_admin();
	for ((name, chain) in tokens) {
		create app.token(name, chain);
	}
}

operation register_master_wallet(address: text) {
	app.require_admin();
	app.register_master_wallet(address);
}

operation link_wallet(address: text, master_address: text) {
	app.require_admin();
	app.link_wallet(address, master_address);
}

operation unlink_wallet(address: text) {
	app.require_admin();		
	app.unlink_wallet(app.Wallet(address));
}

operation delete_wallet(address: text) {
  app.require_admin();
  app.delete_wallet(app.Wallet(address));
}

operation create_lottery(
	participation_rules: list<app.participation_rule>,
	ticket_rules: list<app.ticket_rule>, 
	rewards: list<app.reward_spec>, 
	description: text,
	draw_time: timestamp,
	app.lottery_type_details,
	app.evm_details
) {
	app.require_admin();
	app.create_lottery(
		participation_rules, 
		ticket_rules, 
		rewards, 
		description, 
		draw_time,
		lottery_type_details,
		evm_details
	);	
}

operation update_lottery(
	participation_rules: list<app.participation_rule>,
	ticket_rules: list<app.ticket_rule>, 
	rewards: list<app.reward_spec>, 
	description: text,
	draw_time: timestamp
) {
	app.require_admin();
	app.update_lottery(
		get_active_lottery(),
		participation_rules, 
		ticket_rules, 
		rewards, 
		description, 
		draw_time
	);		
}

operation set_lottery_state(state: text) {
	app.require_admin();	
	app.set_lottery_state(get_active_lottery(), app.lottery_state.value(state));
}

operation update_lottery_description(description: text) {
	app.require_admin();
	app.update_lottery_description(get_active_lottery(), description);
}

operation update_lottery_draw_time(draw_time: timestamp) {
	app.require_admin();
	app.update_lottery_draw_time(get_active_lottery(), draw_time);
}

operation update_lottery_evm_details(lottery_id: integer, app.evm_details) {
	app.require_admin();
	app.update_lottery_evm_details(app.Lottery(lottery_id), evm_details);
}

operation create_new_snapshotting_round(time: timestamp) {
	app.require_admin();
	app.create_new_snapshotting_round(get_active_lottery(), time);
}

operation save_wallet_snapshot(address: text, balances: list<app.token_balance>) {
	app.require_admin();
	app.save_wallet_snapshot(get_active_lottery(), app.Wallet(address), balances);	
}

operation issue_tickets(count: integer = integer.MAX_VALUE) {
	app.require_admin();	
	app.issue_tickets(get_active_lottery(), count);
}

operation draw(last_snapshot_round_id: integer, batch_size: integer = integer.MAX_VALUE) {
	// be careful and make sure code is reliable and doesn't throw errors, otherwise we will not be able to perform draw 
	app.require_admin();
	val lottery = get_active_lottery();
	val last_snapshot_round = app.SnapshotRound(lottery, last_snapshot_round_id);
	app.draw(lottery, last_snapshot_round, batch_size);
}

operation save_snapshot_stats_for_all_lotteries() {
	app.require_admin();
	val lotteries = app.get_lotteries_without_snapshot_stats();
	
	for (lottery in lotteries) {
		app.save_lottery_snapshot_stats(lottery);
	}
}

operation save_snapshot_stats(lottery_id: integer, round: integer) {
	app.require_admin();
	app.save_snapshot_stats(app.snapshot_round @ { .lottery.id == lottery_id, .id == round });
}

operation insert_bonus_ticket_snapshot_round_to_active_lottery(lottery_ids: list<integer>, amount: integer, time: timestamp) {
	app.require_admin();
	val lotteries = list<app.lottery>();
	for (id in lottery_ids) {
		lotteries.add(app.Lottery(id));
	}
	app.insert_bonus_tickets_snapshot_round(lotteries, get_active_lottery(), amount, time);
}

operation issue_non_snapshot_tickets(wallet_tickets: list<app.non_snapshot_wallet_tickets>) {
    app.require_admin();
    app.issue_non_snapshot_tickets(get_active_lottery(), wallet_tickets);
}
