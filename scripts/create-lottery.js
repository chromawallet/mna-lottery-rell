require('dotenv').config()

const ParticipationRule = require("../js/model/participation-rule");
const TicketRule = require("../js/model/ticket-rule");
const EVMLotteryDetails = require("../js/model/evm-lottery-details");
const LotteryTypeDetails = require("../js/model/lottery-type-details");
const LotteryType = require("../js/model/lottery-type");
const { PlotList, PlotRange } = require("../js/model/reward-spec");

const { getLotteryService } = require("./utils");

(async () => {
  const service = await getLotteryService();

  try {
    await service.createLottery(
      [
        new ParticipationRule("DAR", "BSC", 100)
      ],
      [
        new TicketRule("DAR", "BSC", 1, 1),
        new TicketRule("UNISWAP-DAR-WETH", "ETHEREUM", 1, 1),
        new TicketRule("PANCAKE-DAR-WBNB", "BSC", 1, 1),
      ],
      [
        new PlotList(["10cd14", "10d0fc", "10d4e4", "10d8cc", "10dcb4", "10e09c", "10e484", "10e86c", "10ec54", "10f03c", "10f424", "10f80c", "10fbf4", "10ffdc", "1103c4", "1107ac"]),
        new PlotList(["10cd15", "10d0fd", "10d4e5", "10d8cd", "10dcb5", "10e09d", "10e485", "10e86d", "10ec55", "10f03d", "10f425", "10f80d", "10fbf5", "10ffdd", "1103c5", "1107ad"]),
        new PlotList(["10cd16", "10d0fe", "10d4e6", "10d8ce", "10dcb6", "10e09e", "10e486", "10e86e", "10ec56", "10f03e", "10f426", "10f80e", "10fbf6", "10ffde", "1103c6", "1107ae"]),
        new PlotList(["10cd17", "10d0ff", "10d4e7", "10d8cf", "10dcb7", "10e09f", "10e487", "10e86f", "10ec57", "10f03f", "10f427", "10f80f", "10fbf7", "10ffdf", "1103c7", "1107af"]),
        new PlotList(["10cd18", "10d100", "10d4e8", "10d8d0", "10dcb8", "10e0a0", "10e488", "10e870", "10ec58", "10f040", "10f428", "10f810", "10fbf8", "10ffe0", "1103c8", "1107b0"]),
        new PlotList(["10cd19", "10d101", "10d4e9", "10d8d1", "10dcb9", "10e0a1", "10e489", "10e871", "10ec59", "10f041", "10f429", "10f811", "10fbf9", "10ffe1", "1103c9", "1107b1"]),
        new PlotList(["10cd1a", "10d102", "10d4ea", "10d8d2", "10dcba", "10e0a2", "10e48a", "10e872", "10ec5a", "10f042", "10f42a", "10f812", "10fbfa", "10ffe2", "1103ca", "1107b2"]),
        new PlotList(["10cd1b", "10d103", "10d4eb", "10d8d3", "10dcbb", "10e0a3", "10e48b", "10e873", "10ec5b", "10f043", "10f42b", "10f813", "10fbfb", "10ffe3", "1103cb", "1107b3"]),
        new PlotList(["10cd1c", "10d104", "10d4ec", "10d8d4", "10dcbc", "10e0a4", "10e48c", "10e874", "10ec5c", "10f044", "10f42c", "10f814", "10fbfc", "10ffe4", "1103cc", "1107b4"]),
        new PlotList(["10cd1d", "10d105", "10d4ed", "10d8d5", "10dcbd", "10e0a5", "10e48d", "10e875", "10ec5d", "10f045", "10f42d", "10f815", "10fbfd", "10ffe5", "1103cd", "1107b5"]),
        new PlotList(["10cd1e", "10d106", "10d4ee", "10d8d6", "10dcbe", "10e0a6", "10e48e", "10e876", "10ec5e", "10f046", "10f42e", "10f816", "10fbfe", "10ffe6", "1103ce", "1107b6"]),
        new PlotList(["10cd1f", "10d107", "10d4ef", "10d8d7", "10dcbf", "10e0a7", "10e48f", "10e877", "10ec5f", "10f047", "10f42f", "10f817", "10fbff", "10ffe7", "1103cf", "1107b7"]),
        new PlotList(["10cd20", "10d108", "10d4f0", "10d8d8", "10dcc0", "10e0a8", "10e490", "10e878", "10ec60", "10f048", "10f430", "10f818", "10fc00", "10ffe8", "1103d0", "1107b8"]),
        new PlotList(["10cd21", "10d109", "10d4f1", "10d8d9", "10dcc1", "10e0a9", "10e491", "10e879", "10ec61", "10f049", "10f431", "10f819", "10fc01", "10ffe9", "1103d1", "1107b9"]),
        new PlotList(["10cd22", "10d10a", "10d4f2", "10d8da", "10dcc2", "10e0aa", "10e492", "10e87a", "10ec62", "10f04a", "10f432", "10f81a", "10fc02", "10ffea", "1103d2", "1107ba"]),
        new PlotList(["125369", "125751", "125b39", "125f21", "126309", "1266f1", "126ad9", "126ec1", "1272a9", "127691", "127a79", "127e61", "128249", "128631", "128a19", "128e01"]),
        new PlotList(["12536a", "125752", "125b3a", "125f22", "12630a", "1266f2", "126ada", "126ec2", "1272aa", "127692", "127a7a", "127e62", "12824a", "128632", "128a1a", "128e02"]),
        new PlotList(["12536b", "125753", "125b3b", "125f23", "12630b", "1266f3", "126adb", "126ec3", "1272ab", "127693", "127a7b", "127e63", "12824b", "128633", "128a1b", "128e03"]),
        new PlotList(["12536c", "125754", "125b3c", "125f24", "12630c", "1266f4", "126adc", "126ec4", "1272ac", "127694", "127a7c", "127e64", "12824c", "128634", "128a1c", "128e04"]),
        new PlotList(["12536d", "125755", "125b3d", "125f25", "12630d", "1266f5", "126add", "126ec5", "1272ad", "127695", "127a7d", "127e65", "12824d", "128635", "128a1d", "128e05"]),
        new PlotList(["12536e", "125756", "125b3e", "125f26", "12630e", "1266f6", "126ade", "126ec6", "1272ae", "127696", "127a7e", "127e66", "12824e", "128636", "128a1e", "128e06"]),
        new PlotList(["12536f", "125757", "125b3f", "125f27", "12630f", "1266f7", "126adf", "126ec7", "1272af", "127697", "127a7f", "127e67", "12824f", "128637", "128a1f", "128e07"]),
        new PlotList(["125370", "125758", "125b40", "125f28", "126310", "1266f8", "126ae0", "126ec8", "1272b0", "127698", "127a80", "127e68", "128250", "128638", "128a20", "128e08"]),
        new PlotList(["125371", "125759", "125b41", "125f29", "126311", "1266f9", "126ae1", "126ec9", "1272b1", "127699", "127a81", "127e69", "128251", "128639", "128a21", "128e09"]),
        new PlotList(["125372", "12575a", "125b42", "125f2a", "126312", "1266fa", "126ae2", "126eca", "1272b2", "12769a", "127a82", "127e6a", "128252", "12863a", "128a22", "128e0a"]),
        new PlotList(["125373", "12575b", "125b43", "125f2b", "126313", "1266fb", "126ae3", "126ecb", "1272b3", "12769b", "127a83", "127e6b", "128253", "12863b", "128a23", "128e0b"]),
        new PlotList(["125374", "12575c", "125b44", "125f2c", "126314", "1266fc", "126ae4", "126ecc", "1272b4", "12769c", "127a84", "127e6c", "128254", "12863c", "128a24", "128e0c"]),
        new PlotList(["125375", "12575d", "125b45", "125f2d", "126315", "1266fd", "126ae5", "126ecd", "1272b5", "12769d", "127a85", "127e6d", "128255", "12863d", "128a25", "128e0d"]),
        new PlotList(["125376", "12575e", "125b46", "125f2e", "126316", "1266fe", "126ae6", "126ece", "1272b6", "12769e", "127a86", "127e6e", "128256", "12863e", "128a26", "128e0e"]),
        new PlotList(["125377", "12575f", "125b47", "125f2f", "126317", "1266ff", "126ae7", "126ecf", "1272b7", "12769f", "127a87", "127e6f", "128257", "12863f", "128a27", "128e0f"]),
        new PlotList(["125378", "125760", "125b48", "125f30", "126318", "126700", "126ae8", "126ed0", "1272b8", "1276a0", "127a88", "127e70", "128258", "128640", "128a28", "128e10"]),
        new PlotList(["125379", "125761", "125b49", "125f32"]),
      ],
      "DAR Lottery",
        1647860400000,
      new LotteryTypeDetails(LotteryType.NftHolderExclusive, "Mining-Ape"),
      new EVMLotteryDetails(1, "BSC", "0x9BBb6C93FAEc023b99eE0a5b43f300EC3c2f41A0")
    );

    console.log("Lottery created");
  } catch (error) {
    console.log(`Error creating lottery: ${error.message}`)
  }
})()
