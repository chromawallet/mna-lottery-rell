require("dotenv").config();

const Token = require("../js/model/token");
const { getLotteryService } = require("./utils");

(async () => {
  const service = await getLotteryService();

  try {
    await service.registerTokens([
      new Token("DAR", "BSC"),
      new Token("UNISWAP-DAR-WETH", "ETHEREUM"),
      new Token("PANCAKE-DAR-WBNB", "BSC"),
    ]);

    console.log("Tokens registered");
  } catch (error) {
    console.error(`Error registering tokens: ${error.message}`);
  }
})()
