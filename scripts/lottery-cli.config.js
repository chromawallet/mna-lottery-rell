
module.exports = {
    environments: {
        local: {
            url: "http://localhost:7740",
            iid: 0,
            adminPrivKey: process.env.ADMIN_PRIV_KEY
        },
        dev: {
            url: "https://mn1z8bnk8g.execute-api.eu-central-1.amazonaws.com/mna-dev/",
            brid: "4EB1CE73CDF2BE4D6DFD2D47DEE1B78F61C1903F4CA5617CA84C0FB76639A42A",
            adminPrivKey: process.env.ADMIN_PRIV_KEY
        },
        dev2: {
            url: "https://mn1z8bnk8g.execute-api.eu-central-1.amazonaws.com/mna-dev/",
            brid: "E042ECC9F56C657162B97F42619B1E5B8CFBDD409985889EF7D12ECE477B8204",
            adminPrivKey: process.env.ADMIN_PRIV_KEY
        },
        prod: {
            url: "https://qmgooh1jx7.execute-api.eu-central-1.amazonaws.com/prod/",
            brid: "01A1CBC11BE89C6E28BD7699EB0FC7443125FA0D2C643D49F7265C8AC91F7E9A",
            adminPrivKey: process.env.ADMIN_PRIV_KEY_PROD
        },
        "dev-mod": {
            url: "https://1fiwlob0e5.execute-api.eu-west-1.amazonaws.com/prod/",
            brid: "3F20C47D3E4E37CDD813C74F332BE8B6EF1C407111EEC8C90759149166F2E11F",
            adminPrivKey: process.env.ADMIN_PRIV_KEY
        },
        "prod-mod": {
            url: "http://54.173.227.116:7740",
            brid: "C5FD94DE7D53EB06367DBE624676E56F34AC7B6B35F993B4386ADBA32B038AA1",
            adminPrivKey: process.env.ADMIN_PRIV_KEY_MOD_PROD
        }
    }
}
