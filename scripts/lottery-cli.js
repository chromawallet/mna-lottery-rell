require("dotenv").config();

const Token = require("../js/model/token");
const LotteryState = require("../js/model/lottery-state");
const TicketRule = require("../js/model/ticket-rule");
const {PlotCount} = require("../js/model/reward-spec");
const {PlotList} = require("../js/model/reward-spec");
const {PlotRange} = require("../js/model/reward-spec");
const {getLotteryService} = require("./utils");
const prompt = require("prompt-sync")({sigint: true});

let service = null;
let registeredTokens = [];

class MenuItem {
  constructor(command, message, action) {
    this.command = command
    this.message = message
    this.action = action
  }
}

class Menu {
  constructor(...items) {
    this.items = items
  }

  async prompt() {
    console.log("----------------------------------");
    this.items.forEach(item => {
      console.log(`(${item.command})\t ${item.message}`)
    });
    const command = prompt("Please select your choice: ");

    const item = this.items.find(item => item.command === command);
    if (item) {
      await item.action();
    } else {
      console.log(`Unknown command: ${command}. Try again.\n`);
    }
  }
}

const registerToken = new MenuItem(
  "rtok",
  "Register new token",
  async () => {
    const tokenName = prompt("Enter token name: ");
    const tokenChain = prompt("Enter token chain: ");

    const token = new Token(tokenName, tokenChain);
    await service.registerTokens([token]);

    console.log("Token registered!\n");
  }
);

const createLottery = new MenuItem(
  "clot",
  "Create new lottery",
  async () => {
    const rules = [];
    const rewards = [];
    const description = prompt("Please provide description: ");
    console.log("Enter lottery rules: ");

    let input = "Y";
    do {
      const tokenName = prompt("Token name: ");
      const tokenChain = prompt("Token chain: ");
      const tokenCount = parseInt(prompt("Token count: "));
      const ticketCount = parseInt(prompt("Ticket count: "));

      const token = registeredTokens.find(token =>
        token.name === tokenName && token.chain === tokenChain
      );

      if (!token) {
        input = prompt(`Token ${tokenName}/${tokenChain} is not registered. Try again? (Y/n) `, "Y");
      } else {
        const tokenRule = new TicketRule(tokenName, tokenChain, tokenCount, ticketCount);
        rules.push(tokenRule);
        input = prompt("Would you like to enter more rules? (Y/n) ", "Y");
      }
    } while (input === "Y");

    console.log(rules);

    console.log("Enter rewards: ");
    let moreRewards = "Y";
    do {
      const type = prompt("Specify rewards as list or range? (range/list/count) ");
      switch (type) {
        case "range":
          const start = parseInt(prompt("Range start: "));
          const end = parseInt(prompt("Range end: "));
          rewards.push(new PlotRange(start, end))
          break;
        case "list":
          const list = prompt("List: ");
          const ids = list.split(" ").map(item => item.trim());
          rewards.push(new PlotList(ids));
          break;
        case "count":
          const count = parseInt(prompt("Plot count: "));
          rewards.push(new PlotCount(count));
          break;
      }
      moreRewards = prompt("Would you like to enter more rewards? (Y/n) ", "Y");
    } while (moreRewards === "Y");

    console.log("Lottery summary")
    console.log("------------------------------------");
    console.log("Description: ");
    console.log(` - ${description}`);
    console.log("Rules: ");
    rules.forEach(rule => console.log(` - ${rule.toString()}`));
    console.log("Rewards: ")
    rewards.forEach(reward => console.log(` - ${reward.toString()}`));

    if (prompt("Do you want to create lottery? (Y/n) ", "Y") === "Y") {
      await service.createLottery(rules, rewards, description);
      console.log("Lottery created\n");
    }
  }
);

const listTokens = new MenuItem(
  "tok",
  "List registered tokens",
  async () => {
    const tokens = await service.getRegisteredTokens();

    if (tokens.length === 0) {
      console.log("No registered tokens\n");
    } else {
      console.log("TOKENS: ");
      console.log("----------------------------------");
      tokens.forEach((token, index) =>
        console.log(`${index + 1}. Name: '${token.name}', Chain: '${token.chain}'`)
      );
      console.log("\n");
    }
  }
);

const createSnapshottingRound = new MenuItem(
  "csnap",
  "Create new snapshotting round",
  async () => {
    const timestamp = prompt("Please enter snapshot time: ");
    await service.createNewSnapshottingRound(parseInt(timestamp));
    console.log("Snapshotting round created");
  }
)

const enableSnapshotting = new MenuItem(
  "snap",
  "Enable snapshotting",
  async () => {
    await service.setLotteryState(LotteryState.Snapshotting);
    console.log("Snapshotting enabled");
  }
);

const finishSnapshotting = new MenuItem(
  "fsnap",
  "Complete snapshotting",
  async () => {
    await service.setLotteryState(LotteryState.SnapshottingFinished);
    console.log("Snapshotting round finished");
  }
);

const pauseSnapshotting = new MenuItem(
  "pause",
  "Pause snapshotting",
  async () => {
    await service.setLotteryState(LotteryState.Paused);
    console.log("Snapshotting paused");
  }
);

const cancelLottery = new MenuItem(
  "canc",
  "Cancel lottery",
  async () => {
    await service.setLotteryState(LotteryState.Canceled);
    console.log("Lottery canceled");
  }
);

const lotteryDetails = new MenuItem(
  "lott",
  "Lottery details",
  async () => {
    const lottery = await service.getActiveLotteryDetails();
    console.log(lottery);
  }
);

const enableDraw = new MenuItem(
  "edraw",
  "Enable draw",
  async () => {
    await service.setLotteryState(LotteryState.WaitingDraw);
  }
)

const draw = new MenuItem(
  "draw",
  "Draw",
  async () => {
    await service.draw();
  }
)

const finishLottery = new MenuItem(
  "end",
  "End lottery",
  async () => {
    await service.setLotteryState(LotteryState.Finished);
    console.log("Lottery completed");
  }
)

const quit = new MenuItem(
  "quit",
  "Quit",
  async () => {
    process.exit(0);
  }
);

const saveSnapshotStats = new MenuItem(
  "ssnap",
  "Saves snapshot stats for old lotteries",
  async () => {
    await service.saveSnapshotStatsForAllLotteries();
    console.log("Snapshots for old lotteries saved");
  }
)

const statesMenuItems = {
  [LotteryState.Created]: [
    lotteryDetails,
    createSnapshottingRound,
    cancelLottery
  ],
  [LotteryState.WaitingSnapshot]: [
    enableSnapshotting,
    cancelLottery
  ],
  [LotteryState.Snapshotting]: [
    lotteryDetails,
    pauseSnapshotting,
    finishSnapshotting,
    cancelLottery
  ],
  [LotteryState.SnapshottingFinished]: [
    lotteryDetails,
    createSnapshottingRound,
    enableDraw,
    cancelLottery,
  ],
  [LotteryState.WaitingDraw]: [
    lotteryDetails,
    draw,
    cancelLottery,
  ],
  [LotteryState.DrawFinished]: [
    lotteryDetails,
    finishLottery
  ],
  [LotteryState.Paused]: [
    lotteryDetails,
    enableSnapshotting,
    finishSnapshotting,
    cancelLottery
  ],
};

(async () => {
  service = await getLotteryService();

  while (true) {
    registeredTokens = await service.getRegisteredTokens();
    const lottery = await service.getActiveLotteryDetails();

    const menuItems = lottery === null
      ? [createLottery]
      : statesMenuItems[lottery.state]

    const menu = new Menu(
      listTokens,
      registerToken,
      saveSnapshotStats,
      ...menuItems,
      quit
    );

    await menu.prompt();
  }
})()
