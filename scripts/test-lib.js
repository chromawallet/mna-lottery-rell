const { runModuleTests } = require('./run-tests');
const http = require('http');

http.get('http://127.0.0.1:7740/brid/iid_0', response => {
    response.on('data', async brid => {
        await runModuleTests(
            'tests',
            'http://127.0.0.1:7740',
            brid.toString()
        );
    })
}).on('error', error => {
    console.log(error);
});